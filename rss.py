import feedparser
import time, httplib2, apiclient.discovery, pytz
from oauth2client.service_account import ServiceAccountCredentials
import sqlite3
from datetime import datetime
import mysql.connector

conn = sqlite3.connect("titles.db")
cursor = conn.cursor()

mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  passwd="yourpass",
database="news"
)

mycursor = mydb.cursor(buffered=True)

CREDENTIALS_FILE = 'Trevor.json'
# spreadsheet_id = '1Fqtx3txMufYco1ZOhQvcP8p_2BDN_QZl1CHr2mkSxMQ' # Alex' table
spreadsheet_id = '1N3pjQv5daAG2Sm9GaZiJrvghE6pbIlcU6j7S3G1O-hI' #Trevor's table

credentials = ServiceAccountCredentials.from_json_keyfile_name(
    CREDENTIALS_FILE,
    ['https://www.googleapis.com/auth/spreadsheets',
     'https://www.googleapis.com/auth/drive'])

httpAuth = credentials.authorize(httplib2.Http())
service = apiclient.discovery.build('sheets', 'v4', http = httpAuth)

keywords =[]
with open('keywords.txt', 'r') as kw:
    words = kw.readlines()
    for word in words:
        keywords.append(word.strip().strip('\n'))

feeds = []
with open('feeds.txt', 'r') as fds:
    feedsRaw = fds.readlines()
    for feed in feedsRaw:
        feeds.append(feed.strip('\n'))

while True:
    all_news = []
    mycursor.execute('SELECT * FROM news;')
    articles = mycursor.fetchall()
    for article in articles:
        if article[0:4] not in all_news:
            title_got = False
            for i in range(len(all_news)):
                if article[0] in all_news[i]:
                    title_got = True
                    break
            if not title_got:
                all_news.append(list(article[0:4]))
        else:
            print('duplicate in sql!')

    for feed in feeds:
        d = feedparser.parse(feed)

        for entry in d.entries:

            if entry:
                for keyword in keywords:

                    if keyword.lower() in entry.title.lower():

                        news = []
                        if feed == 'https://www.theguardian.com/world/rss':
                            feedSource = 'The Guardian'
                        elif feed == 'https://www.scmp.com/rss/91/feed':
                            feedSource = 'South China Morning Post'
                        elif feed in ['https://www.channelnewsasia.com/rssfeeds/8395986',
                                      'https://www.channelnewsasia.com/rssfeeds/8395744',
                                      'https://www.channelnewsasia.com/rssfeeds/8396082',
                                      'https://www.channelnewsasia.com/rssfeeds/8395884']:
                            feedSource = 'Channel NewsAsia'
                        elif feed in ['https://www.straitstimes.com/news/world/rss.xml',
                                      'https://www.straitstimes.com/news/business/rss.xml',
                                      'https://www.straitstimes.com/news/sport/rss.xml',
                                      'https://www.straitstimes.com/news/lifestyle/rss.xml',
                                      'https://www.straitstimes.com/news/opinion/rss.xml',
                                      'https://www.straitstimes.com/news/singapore/rss.xml',
                                      'https://www.straitstimes.com/news/politics/rss.xml',
                                      'https://www.straitstimes.com/news/asia/rss.xml',
                                      'https://www.straitstimes.com/news/tech/rss.xml',
                                      'https://www.straitstimes.com/news/forum/rss.xml',
                                      'https://www.straitstimes.com/news/multimedia/rss.xml']:
                            feedSource = 'The Straits Times'
                        elif feed == 'https://mothership.sg/feed/':
                            feedSource = 'Mothership'
                        elif feed == 'https://www.theonlinecitizen.com/feed/':
                            feedSource = 'The Online Citizen'
                        elif feed in ['https://www.todayonline.com/feed/singapore',
                                      'https://www.todayonline.com/feed/world']:
                            feedSource = 'Today'
                        elif feed == 'http://www.theindependent.sg/feed':
                            feedSource = 'The Independent Singapore'
                        elif feed == 'https://rss.nytimes.com/services/xml/rss/nyt/World.xml':
                            feedSource = 'The New York Times'
                        else:
                            feedSource = 'unknown source'

                        news.append(entry.title)
                        news.append(entry.links[0].href)
                        # print(entry.published_parsed)

                        utc_dt = datetime.utcfromtimestamp(time.mktime(entry.published_parsed)).replace(tzinfo=pytz.utc)
                        # convert it to tz
                        tz = pytz.timezone('Asia/Singapore')
                        dt = utc_dt.astimezone(tz)
                        # print(str(dt) + ' SGT')
                        # print()
                        news.append(str(dt) + ' SGT')

                        news.append(feedSource)

                        if news not in all_news:
                            if len(news) == 4:
                                mycursor.execute('SELECT 1 FROM news WHERE Title = %s;', ((news[0],)))
                                article_exists = mycursor.fetchone() is not None
                                if not article_exists:
                                    print('new!')
                                    sql = "INSERT INTO news (Title, Source_Link, Time_of_the_post, Source_Name) VALUES (%s, %s, %s, %s)"
                                    mycursor.execute(sql, article[0:4])
                                    mydb.commit()
                                    all_news.append(news[0:4])
                                    print(str(len(all_news)) + ' ' + feedSource)

        # Writing to the table
    print(len(all_news))
    columnsName = [['Title', 'Source Link', 'Time of the post', 'Source Name']]
    values = service.spreadsheets().values().batchUpdate(
        spreadsheetId=spreadsheet_id,
        body={
            "valueInputOption": "USER_ENTERED",
            "data": [
                {"range": "A1:D1",
                 "majorDimension": "ROWS",
                 "values": columnsName},
                {"range": "A2:D"+str(len(all_news)+1),
                 "majorDimension": "ROWS",
                 "values": all_news},
            ]
        }
    ).execute()
    print('printed!')
    time.sleep(60*60*24/30)


