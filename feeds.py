feeds = ['https://www.theguardian.com/world/rss', 'https://www.scmp.com/rss/91/feed',
         'https://rss.nytimes.com/services/xml/rss/nyt/World.xml',
         #straitstimes
         'https://www.straitstimes.com/news/world/rss.xml', 'https://www.straitstimes.com/news/business/rss.xml',
         'https://www.straitstimes.com/news/sport/rss.xml', 'https://www.straitstimes.com/news/lifestyle/rss.xml',
         'https://www.straitstimes.com/news/opinion/rss.xml', 'https://www.straitstimes.com/news/singapore/rss.xml',
         'https://www.straitstimes.com/news/politics/rss.xml', 'https://www.straitstimes.com/news/asia/rss.xml',
         'https://www.straitstimes.com/news/tech/rss.xml', 'https://www.straitstimes.com/news/forum/rss.xml',
         'https://www.straitstimes.com/news/multimedia/rss.xml',
         #ChannelNewAsia
         'https://www.channelnewsasia.com/rssfeeds/8395986', 'https://www.channelnewsasia.com/rssfeeds/8395744',
         'https://www.channelnewsasia.com/rssfeeds/8396082', 'https://www.channelnewsasia.com/rssfeeds/8395884',
         #Mothership
         'https://mothership.sg/feed/',
         #Today
         'https://www.todayonline.com/hot-news/feed',
         #The Online Citizen
         'https://www.theonlinecitizen.com/feed/',
         #Independent.SG
         'http://www.theindependent.sg/feed',
         ]

with open('feeds.txt', 'w') as fds:
    for row in feeds:
        fds.write(row.strip().strip("'") + '\n')

