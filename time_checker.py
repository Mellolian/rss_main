import feedparser
import time, httplib2, apiclient.discovery, pytz
from oauth2client.service_account import ServiceAccountCredentials
import sqlite3
from datetime import datetime

conn = sqlite3.connect("titles.db")
cursor = conn.cursor()

while True:
    all_news = []
    cursor.execute('SELECT * FROM news;')
    articles = cursor.fetchall()
    for article in articles:
        if article[0:4] not in all_news:
            title_got = False
            for i in range(len(all_news)):
                if article[0] in all_news[i]:
                    title_got = True
                    break
            if not title_got:
                all_news.append(list(article[0:4]))
                # print(list(article[0:4]))
        else:
            print('duplicate in sql!')

    for news in all_news:
        if 'SGT' not in news[2]:
            # print(news)
            article_time = news[2].split(' ')

            day = int(article_time[1])

            if article_time[2] == 'Feb':
                month = 2
            elif article_time[2] == 'Jan':
                month = 1

            year = article_time[3]

            old_time = article_time[4].split(':')
            hour = int(old_time[0])
            minute = old_time[1]
            second = old_time[2]


            tz = article_time[5]

            if tz =='+0000' or tz == 'GMT':
                hour += 8
                if hour >= 24:
                    day += 1
                    if day > 31:
                        day = 1
                        month += 1
                    hour -= 24

                tz = '+08:00 SGT'

            elif tz == '+0800':
                tz = '+08:00 SGT'

            if len(str(hour)) == 1:
                hour = '0' + str(hour)

            if len(str(day)) == 1:
                day = '0' + str(day)

            new_time = [str(hour), minute, second]
            new_date = [year, '0'+str(month), str(day)]
            news = list(news)
            news[2] = (('-'.join(new_date))+ ' ' +(':'.join(new_time)) +tz)

            # if 'SGT' not in news[2]:
            print(news)

            if len(news) == 4:
                cursor.execute('SELECT 1 FROM news WHERE Title = ?;', ((news[0],)))
                article_exists = cursor.fetchone() is not None
                if not article_exists:
                    print('new!')
                    cursor.execute("INSERT INTO news VALUES (?,?,?,?)",
                                   news[0:4])
                    conn.commit()
                else:
                    cursor.execute('DELETE FROM news WHERE Title = ?;', ((news[0],)))
                    cursor.execute("INSERT INTO news VALUES (?,?,?,?)",
                                   news[0:4])
                    conn.commit()


    print('printed!')
    time.sleep(60*60*24/30)
