import feedparser
import time, httplib2, apiclient.discovery, pytz
from oauth2client.service_account import ServiceAccountCredentials
import sqlite3
from datetime import datetime

conn = sqlite3.connect("titles.db")
cursor = conn.cursor()

while True:
    all_news = []
    cursor.execute('SELECT * FROM news;')
    articles = cursor.fetchall()
    for article in articles:
        if article[0:4] not in all_news:
            title_got = False
            for i in range(len(all_news)):
                if article[0] in all_news[i]:
                    title_got = True
                    break
            if not title_got:
                all_news.append(list(article[0:4]))
                # print(list(article[0:4]))
        else:
            print('duplicate in sql!')

    for news in all_news:
        if 'unknown source' in news[3]:
            news[3] = 'Today'
            print(news)


            if len(news) == 4:
                cursor.execute('SELECT 1 FROM news WHERE Title = ?;', ((news[0],)))
                article_exists = cursor.fetchone() is not None
                if not article_exists:
                    print('new!')
                    cursor.execute("INSERT INTO news VALUES (?,?,?,?)",
                                   news[0:4])
                    conn.commit()
                else:
                    cursor.execute('DELETE FROM news WHERE Title = ?;', ((news[0],)))
                    cursor.execute("INSERT INTO news VALUES (?,?,?,?)",
                                   news[0:4])
                    conn.commit()


    print('printed!')
    time.sleep(60*60*24/30)
